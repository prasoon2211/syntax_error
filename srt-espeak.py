import pysrt
from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def to_milliseconds(t):
    return t.hours * (3600 * 1000) + t.minutes * (60 * 1000) + \
        t.seconds * 1000 + t.milliseconds

def removeNonAscii(s):
    return "".join(filter(lambda x: ord(x)<128, s))


import ipdb;ipdb.set_trace()

final_str = ''
"""
f = open('sample.srt', 'r+')
f.write(removeNonAscii(f.read()))
"""
subs = pysrt.open('spanish.txt')

for i, sub in enumerate(subs):
    text = strip_tags(sub.text)
    try:
        next_start = subs[i+1].start
    except Exception as e:
        print e
        final_str +=' ' + sub.text + '.'
        break
    break_duration = next_start - sub.end
    break_duration = to_milliseconds(break_duration)
    final_str += sub.text + '<break time="' + unicode(break_duration) + 'ms"/>'

print final_str
